import Faker from 'faker';
import uuid from 'react-uuid';

const template = {
    totalImages: 712,
    spritesInRow: 27,
    sprite: {
        width: 32,
        height: 32
    }
}

const getDefaultProjects = (howMany = 10) => {
    let result = [];
    while(howMany--) {
        const xPosition = howMany % template.spritesInRow;
        const yPosition = Math.floor(howMany / template.spritesInRow)
        const xOffset = '-' + xPosition * template.sprite.width + 'px';
        const yOffset = '-' + yPosition * template.sprite.height + 'px';
        let project = Object.create(null);

        project.id = uuid();
        project.name = Faker.address.country() + '/' + Faker.address.cityName();
        project.description = Faker.lorem.paragraph();
        project.logoSpritePosition = '' + xOffset + ' ' + yOffset;

        result.push(project);
    }
    return result;
}

export default getDefaultProjects;
