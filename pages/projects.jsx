import bluebird from 'bluebird';
import RedisLib from 'redis';
import getDefaultProjects from "./seeders/ProjectSeeder";

export const getStaticProps = async () => {
    bluebird.promisifyAll(RedisLib.RedisClient.prototype);
    const redis = RedisLib.createClient();
    let data = {projects: []};

    redis.on("error", function (error) {
        console.error("redis:", error);
    });

    await redis.existsAsync('projects').then(async reply => {
        const isProjectListEmpty = reply !== 1;
        if (isProjectListEmpty) {
            console.error('No data stored in redis: setting some from seeder');
            data.projects = getDefaultProjects(712);
            await redis.set('projects', JSON.stringify(data.projects));
        } else {
            data.projects = JSON.parse(await redis.getAsync('projects'));
        }
    });

    return {
        props: {
            projects: data.projects,
        },
    };
};

const Projects = ({projects}) => (
    <>
        <article className="hero">
            <h1>HealthCare.gov Articles:</h1>
            <table>
                <thead>
                <tr>
                    <th>Logo</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                {projects && projects.length > 0 && projects.map((project) => (
                    <tr key={project.id}>
                        <td>
                            <img src="sprites/sprites.png"
                                 className="logo"
                                 style={{objectPosition: '' + project.logoSpritePosition}}/>
                        </td>
                        <td>{project.name}</td>
                        <td>{project.description}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </article>

        <style jsx>{`
          .logo {
            cursor: pointer;
            width: 32px;
            height: 32px;
            object-fit: none;
            object-position: 0 0;
            zoom: 2;
            -moz-transform: scale(2);
            -moz-transform-origin: 0 0;
          }
        `}</style>
    </>
);


export default Projects;