import Link from "next/link";


const Home = () => (
    <article>
        <h1>Welcome on my site</h1>
        <h1><Link href="/projects" prefetch={false}>Go To Projects!</Link></h1>
    </article>
);

export default Home;